
//Group Members
//  * Antonio Merendaz - C0741427
//  * Carlos Bulado - C0734506
//  * Giselle Tavares - C0744277

package com.example.droidcafewsettings;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.design.widget.FloatingActionButton;
import android.widget.Button;
import android.widget.EditText;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertNotNull;

@RunWith(RobolectricTestRunner.class)

public class OrderActivityTest
{
    private MainActivity activity1;
    private OrderActivity activity2;
    SharedPreferences sharedPref;

    @Before
    public void setUp() throws Exception
    {
        activity1 = Robolectric.buildActivity( MainActivity.class )
                .create()
                .resume()
                .get();
        activity2 = Robolectric.buildActivity( OrderActivity.class )
                .create()
                .resume()
                .get();
    }

    @Test
    public void shouldNotBeNull() throws Exception
    {
        assertNotNull( activity1 );
        assertNotNull( activity2 );
    }

    @Test
    public void cartPageUserInfoTest() throws Exception {
        // TC2:  Cart page remembers user info
        // Test that the app remembers the information the user entered on the Shopping Cart page.
        // 1. Find the Floating Action Button on MainActivity and click in it
        FloatingActionButton fab = activity1.findViewById(R.id.fab);
        fab.performClick();

        // 2. Find the 3 editTexts and put some text on each one
        // 2a. Name
        EditText etName = activity2.findViewById(R.id.name_text);
        etName.setText("John Smith");

        // 2b. Address
        EditText etAddress = activity2.findViewById(R.id.address_text);
        etAddress.setText("1234, Avenue Street");

        // 2c. Phone
        EditText etPhone = activity2.findViewById(R.id.phone_text);
        etPhone.setText("4163457654");

        // 3. Find the Save Customer info Button and click in it
        Button save = activity2.findViewById(R.id.saveButton);
        save.performClick();

        // 4. Get the data saved on Shared Preferences
        sharedPref  = activity2.getPreferences(Context.MODE_PRIVATE);

        // 4a. Name
        String name = sharedPref.getString("name", "");

        // 4b. Address
        String address = sharedPref.getString("address", "");

        // 4c. Phone
        String phone = sharedPref.getString("phone", "");

        // 5. Check if the info entered in the Edit Texts are the same
        // saved on Shared Preferences
        // 5a. Name
        assertThat(name, equalTo( etName.getText().toString()));

        // 5a. Address
        assertThat(address, equalTo( etAddress.getText().toString()));

        // 5a. Phone
        assertThat(phone, equalTo( etPhone.getText().toString()));
    }
}